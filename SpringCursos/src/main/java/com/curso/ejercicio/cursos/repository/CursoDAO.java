package com.curso.ejercicio.cursos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.curso.ejercicio.cursos.entity.Curso;

public interface CursoDAO extends JpaRepository<Curso, Integer> {

	@Query(value = "Select * from curso where precio>? and precio<?", nativeQuery = true)
	List<Curso> buscarCursosPorPrecio(Double min, Double max);
}
