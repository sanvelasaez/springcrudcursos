package com.curso.ejercicio.cursos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.curso.ejercicio.cursos.entity.Curso;
import com.curso.ejercicio.cursos.service.CursoService;
import com.curso.ejercicio.cursos.service.CurosServiceImpl;

@RestController
public class CursoController {

	@Autowired
	CursoService service;

	// http://localhost:8080/curso
	@GetMapping(value = "curso", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Curso> listarCursos() {
		return service.dameCursos();
	}

	// http://localhost:8080/curso/1
	@GetMapping(value = "curso/{codCurso}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Curso buscaCurso(@PathVariable Integer codCurso) {
		return service.buscarCurso(codCurso);
	}

	// http://localhost:8080/curso
	@PostMapping(value = "curso", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<Curso> altaCurso(@RequestBody Curso curso) {
		return service.altaCurso(curso);
	}

	// http://localhost:8080/curso?codCurso=1&horas=10
	@PutMapping(value = "curso", params = { "codCurso", "horas" })
	public void actualizarCurso(@RequestParam("codCurso") Integer codCurso, @RequestParam("horas") Integer horas) {
		service.actualizarCurso(codCurso, horas);
	}

	// http://localhost:8080/curso/1
	@DeleteMapping(value = "curso/{codCurso}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Curso> eliminarCurso(@PathVariable(name = "codCurso") Integer codCurso) {
		return service.eliminarCurso(codCurso);
	}

	// http://localhost:8080/curso?min=1&max=5
	@GetMapping(value = "curso", params = { "min", "max" }, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Curso> listarCursosPrecio(@RequestParam("min") Double min, @RequestParam("max") Double max) {
		return service.dameCursos(min, max);
	}

}
