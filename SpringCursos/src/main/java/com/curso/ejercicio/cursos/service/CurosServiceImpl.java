package com.curso.ejercicio.cursos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.ejercicio.cursos.entity.Curso;
import com.curso.ejercicio.cursos.repository.CursoDAO;

@Service
public class CurosServiceImpl implements CursoService {

	@Autowired
	CursoDAO dao;

	@Override
	public List<Curso> dameCursos() {
		return dao.findAll();
	}

	@Override
	public Curso buscarCurso(Integer codCurso) {
		return dao.findById(codCurso).orElse(null);
	}

	@Override
	public List<Curso> altaCurso(Curso curso) {
		dao.save(curso);
		return dameCursos();
	}

	@Override
	public void actualizarCurso(Integer codCurso, Integer horas) {
		Curso curso = buscarCurso(codCurso);
		curso.setDuracion(horas);
		dao.save(curso);
	}

	@Override
	public List<Curso> eliminarCurso(Integer codCurso) {
		dao.deleteById(codCurso);
		return dameCursos();
	}

	@Override
	public List<Curso> dameCursos(Double min, Double max) {
		return dao.buscarCursosPorPrecio(min, max);
	}

}
