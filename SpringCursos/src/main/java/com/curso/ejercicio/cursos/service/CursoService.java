package com.curso.ejercicio.cursos.service;

import java.util.List;
import java.util.Optional;

import com.curso.ejercicio.cursos.entity.Curso;

public interface CursoService {

	List<Curso> dameCursos();

	Curso buscarCurso(Integer id);

	List<Curso> altaCurso(Curso curso);

	void actualizarCurso(Integer codCurso, Integer horas);

	List<Curso> eliminarCurso(Integer id);

	List<Curso> dameCursos(Double min, Double max);

}
