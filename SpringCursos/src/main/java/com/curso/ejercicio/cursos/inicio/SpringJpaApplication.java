package com.curso.ejercicio.cursos.inicio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = "com.curso.ejercicio.cursos.entity")
@EnableJpaRepositories(basePackages = "com.curso.ejercicio.cursos.repository")
@SpringBootApplication(scanBasePackages = { "com.curso.ejercicio.cursos.controller",
		"com.curso.ejercicio.cursos.service" })
public class SpringJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJpaApplication.class, args);
	}

}
