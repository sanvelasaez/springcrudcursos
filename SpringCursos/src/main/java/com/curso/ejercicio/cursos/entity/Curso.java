package com.curso.ejercicio.cursos.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity(name = "curso")
public class Curso {

	@Id
	private Integer codCurso;
	private String nombre;
	private Integer duracion;
	private Double precio;
	/**
	 * @param codCurso
	 * @param nombre
	 * @param duracion
	 * @param precio
	 */
	public Curso(Integer codCurso, String nombre, Integer duracion, Double precio) {
		this.codCurso = codCurso;
		this.nombre = nombre;
		this.duracion = duracion;
		this.precio = precio;
	}

	public Curso() {
	}

	/**
	 * @return the codCurso
	 */
	public Integer getCodCurso() {
		return codCurso;
	}

	/**
	 * @param codCurso the codCurso to set
	 */
	public void setCodCurso(Integer codCurso) {
		this.codCurso = codCurso;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the duracion
	 */
	public Integer getDuracion() {
		return duracion;
	}

	/**
	 * @param duracion the duracion to set
	 */
	public void setDuracion(Integer duracion) {
		this.duracion = duracion;
	}

	/**
	 * @return the precio
	 */
	public Double getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Curso [codCurso=" + codCurso + ", nombre=" + nombre + ", duracion=" + duracion + ", precio=" + precio
				+ "]";
	}
	
	
	
}